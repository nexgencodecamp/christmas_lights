import re, os, time, sys, subprocess
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

FILE_PATTERN = "CL-[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}\.py$"
DIRECTORY_CHECKED = "../files/2_checked"
SECURITY_CHECK_PASSED = 300

class Watcher:
    DIRECTORY_TO_WATCH = "../files/1_passed"    

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Error")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print("Received created event - %s." % event.src_path)
            # Code check the file            
            output = Handler.doCodeCheck(event.src_path)
            print("returned from output.... %d" % output)
            if(output == SECURITY_CHECK_PASSED):
                # Move to checked folder
                newPath = event.src_path.replace("1_passed", "2_checked")
                os.rename(event.src_path, newPath)
                print("Moving file %s to CHECKED..." % event.src_path)

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            print("Received modified event - %s." % event.src_path)
        elif event.event_type == 'deleted':
            # Taken any action here when a file is deleted
            print("Received deleted event - %s." % event.src_path)
    
    @staticmethod
    def doCodeCheck(pth):
        try:
            output = subprocess.check_output("bandit ../files/*.py", shell=True, encoding='UTF-8')
            print("Output = %s" % output)
            return 300
        except:
            print("Oops!", sys.exc_info()[1], "occurred.")
            print()
            # Move to rejected folder
            rejPath = pth.replace("1_passed", "3_rejected")
            os.rename(pth, rejPath)
            print("Moving file %s to REJECTED..." % pth)

if __name__ == '__main__':
    w = Watcher()
    w.run()