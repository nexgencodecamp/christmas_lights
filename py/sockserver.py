# first of all import the socket library
import socket            
from contextlib import redirect_stdout

# open a file to write to
f = open('test.py', 'w')

# next create a socket object
s = socket.socket()        
print ("Socket successfully created")
 
# reserve a port on your computer in our
# case it is 12345 but it can be anything
port = 12345               
 
# Next bind to the port
# we have not typed any ip in the ip field
# instead we have inputted an empty string
# this makes the server listen to requests
# coming from other computers on the network
s.bind(('', port))        
print ("socket binded to %s" %(port))
 
# put the socket into listening mode
s.listen(5)    
print ("socket is listening")           
 
# a forever loop until we interrupt it or
# an error occurs

# Establish connection with client.
c, addr = s.accept()    
print ('Got connection from', addr )

while True: 
  msg = c.recv(1024).decode('utf-8')
  
  if msg == 'q':
      c.close()
      f.close()
      break
  elif (msg):
    with redirect_stdout(f):
        print(msg, flush=True)
