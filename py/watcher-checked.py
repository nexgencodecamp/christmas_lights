import re
import os
import time
import socket
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

FILE_PATTERN = "CL-[0-9]{4}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}\.py$"
DIRECTORY_PASSED = "../files/1_passed"

s = socket.socket()
port = 12345

class Watcher:
    DIRECTORY_TO_WATCH = "../files/2_checked"

    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(5)
        except:
            self.observer.stop()
            print("Error")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        elif event.event_type == 'created':
            # Take any action here when a file is first created.
            print("Received created event - %s." % event.src_path)
            # Verify the file
            Handler.verifyFile(event.src_path)

        elif event.event_type == 'modified':
            # Taken any action here when a file is modified.
            print("Received modified event - %s." % event.src_path)
        elif event.event_type == 'deleted':
            # Taken any action here when a file is deleted
            print("Received deleted event - %s." % event.src_path)

    @staticmethod
    def verifyFile(pth):
        # If the file conforms to the correct naming scheme, move it to the folder '1_checked'
        match = re.search(FILE_PATTERN, pth)

        if match:
            print("YES! We have a match!")

            # Check that the file is > 0 and < 1000 bytes
            fSizeVerified = os.path.getsize(pth)
            if fSizeVerified:
                # Send the contents to the Raspberry pi through a socket
                # connect to the server on local computer
                s.connect(('192.168.0.173', port))

                # Get msg from file
                msg = ''
                with open(pth) as f:
                    msg = f.read()
                    print(msg)

                # Send msg
                s.send(bytes(msg, 'utf-8'))
                s.close()

                print("Sent file to raspberry pi...")

                # Move the file to the 1_passed folder
                print("Moving file %s to PASSED..." % pth)
                newPath = pth.replace("2_checked", "4_complete")
                os.rename(pth, newPath);
            else:
                # Move the file to the 2_rejected folder
                print("Moving file to REJECTED...")
                rejPath = pth.replace("0_queued", "3_rejected")
                os.rename(pth, rejPath);
        else:
            print("No match")
            # Move the file to the 2_rejected folder

if __name__ == '__main__':
    w = Watcher()
    w.run()