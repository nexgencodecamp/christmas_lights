import sys
import subprocess

try:
    output = subprocess.check_output("bandit ../files/*.py", shell=True, encoding='UTF-8')
except:
    print("Oops!", sys.exc_info()[1], "occurred.")
    print()