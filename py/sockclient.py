# Import socket module
import socket

# Create a socket object
s = socket.socket()

# Define the port on which you want to connect
port = 12345

# connect to the server on local computer
s.connect(('192.168.0.173', port))

# receive data from the server and decoding to get the string.
#print (s.recv(1024).decode())

# the program has connected because it has something to send
#s.send('test = 1')
test_msg = """Learn Python
Programming"""


msg = '';
while (msg != 'Q'):
    msg = input('>')
    if(msg):
        s.send(bytes(msg, 'utf-8'))