var express = require('express');
var router = express.Router();
var date = require('date-and-time');
var currTime = date.format(new Date(), 'YYYY-MM-DD-HH:mm:ss');
var {execSync} = require("child_process");
var ctr = 0;
const fs = require('fs');
const PYTHON_CODE_PREFIX = "CL-";
const PYTHON_CODE_SUFFIX = ".py";
const PYTHON_CODE_PATH = "./files/0_queued/";

router.get('/code', function(req, res, next) {   
    res.render('test_route_code');
});

router.post('/python', function(req, res, next){    
    // Write the python code to a file
    var fName = generateFileName();
    run("echo '" + req.body.code + "' >> " + fName);

    // Check if file exists!
    let statsObj,retCodeFile
    try {
        statsObj = fs.statSync(fName);
        retCodeFile = 100;
    }
    catch(err) {
        retCodeFile = -99;
    }

    // Return page to user
    res.render('test_route_code',{ number: currTime, fileRetCode: retCodeFile });
});

function run(command) {
    try {
        return execSync(command, { encoding: "utf8" });
    }
    catch(err) {        
        return -1
    }
}

/**
 * Use ctr to generate a filename of the format: CL-YYYY-MM-DD-HH-MM-SS.py
 * Where YYYY-MM-DD is the current date
 * HH-MM-SS is the current date
 */
function generateFileName(){
    var t = date.format(new Date(), 'YYYY-MM-DD-HH-mm-ss');
    return PYTHON_CODE_PATH + PYTHON_CODE_PREFIX + t + PYTHON_CODE_SUFFIX;
}

module.exports = router