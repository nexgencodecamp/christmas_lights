var express = require('express');
var router = express.Router();
var date = require('date-and-time');
var currTime = date.format(new Date(), 'YYYY/MM/DD HH:mm:ss');

  /* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { number: currTime });
});

module.exports = router;
